from flask import Flask
from flask_restful import Resource, Api, reqparse
import os
import numpy as N
import fs as filesystem
import json
import time
import redis

from scipy import interpolate
from scipy.spatial import Delaunay

from .utils import IrishGridConverter, in_box
from flask import current_app

from .surge import SurgeRisk
from .earthquake import QuakeRisk
from .volcano import VolcanoConcentricRisk
from .volcano_landlab import VolcanoLavaRisk
from .concentric import ConcentricRisk

_phenomena = { # Dictonary object for storing risks
    'surge-elevation': SurgeRisk,
    'earthquake-shibata-study': QuakeRisk,
    'volcano-ash-deposition': VolcanoConcentricRisk,
    'volcano-lava': VolcanoLavaRisk,
    'concentric': ConcentricRisk
}

def phenomena(which):  # Function for returning a risk based on a choice
    print(which) # Print out choice to screen?
    print(_phenomena) # Print out dict object to screen?
    if which in _phenomena: # If the choice is in the dict do this
        return _phenomena[which]() # Return dict that applied to the choice  creates new instance
    return None # If the choice is not in the dict do this

class ResultService: # This class will return results
    def load(self):
        self._cached_heights = N.load('orp_sim_firstgen/cache/heights.npz')  # Set cached_heights to data from a file

    def retrieve(self, times, risk, risk_parameters, feature_sets, cla, clo, la1, lo1, la2, lo2): #
        converter = IrishGridConverter()

        xx, yy, c = self._cached_heights
        xc = N.array(sorted(set(xx.flatten()))) # flattening into 2D array
        yc = N.array(sorted(set(yy.flatten())))
        x1, y1 = converter(la1, lo1, inverse=False)
        x2, y2 = converter(la2, lo2, inverse=False)

        inside = (x1 < xx) & (y1 < yy) & (x2 > xx) & (y2 > yy)
        xx = xx[inside]
        yy = yy[inside]
        w = c[inside]

        xc = xc[(x1 < xc) & (x2 > xc)]
        yc = yc[(y1 < yc) & (y2 > yc)]

        # Replace with RectBivariateSpline
        #f = interpolate.RectBivariateSpline(yc, xc, z)
        # Should f be called?
        try:
            f = interpolate.interp2d(xx.flatten(), yy.flatten(), w.flatten())
        except:
            f = lambda a, b: 0
        for feature_set in feature_sets.values():
            for feature in feature_set:
                feature.setHeight(f(*converter(inverse=False, *feature.getLatLng()))[0])

        grid = {
            'northWest': converter(x1, y2),
            'southEast': converter(x2, y1),
            'divisionsLat': len(yc),
            'divisionsLon': len(xc)
        }
        xcl = sorted(list(xc))
        ycl = sorted(list(yc))

        if len(w):
            elevations = [(i, j, converter(xcl[i], ycl[j]), w[i + j * len(xcl)]) for i in range(len(xcl)) for j in range(len(ycl))]

        risk.load(current_app.logger, (cla, clo), f(*converter(cla, clo, inverse=False))[0], heights=(xx, yy, w))

        results = []
        for t in times:
            current_app.logger.error(t)
            risk.advance_time(t)

            if len(elevations):
                wrisk = N.array([risk.calculate(x, z, i, j) for i, j, x, z in elevations]).reshape((len(xcl), len(ycl)))
                current_app.logger.error(' - wrisk')
                frisk = interpolate.RectBivariateSpline(xcl, ycl, wrisk)
                current_app.logger.error(' - frisk')
                points = {(len(xcl) - i - 1, len(ycl) - j - 1): ((x, z), wrisk[i, j]) for i, j, x, z in elevations}
                current_app.logger.error(' - points')
                for feature_set in feature_sets.values():
                    for feature in feature_set:
                        feature.updateHealth(t, lambda la, lo: frisk(*converter(la, lo, inverse=False)))
                current_app.logger.error(' - features')

                def test(x, y):
                    return risk.calculate(x, y, f(*converter(y, x, inverse=False))[0])

                results.append([t, points, lambda x, y: test(x, y), grid])
            else:
                results.append([t, [], lambda x, y: 0., grid, {}])

        risk.finish()

        return feature_sets, results


class Feature:
    def __init__(self, id, location):
        self._id = id
        self._location = location
        self._current_health = {}
        self._height = 0

    def getLatLng(self):
        return self._location

    def updateHealth(self, t, f):
        self._current_health[t] = 100. * (1 - f(*self._location)[0][0])

    def setHeight(self, z):
        self._height = z

    def healthDict(self):
        return {
            'h': sorted([(t, h) for t, h in self._current_health.items()])
        }


class Result(Resource):
    step = 3000

    def __init__(self, service, redis): #Constructor
        self._service = service
        self._redis = redis

    def post(self, result_id): # Requests
        current_app.logger.warn('Simulation request received')

        parser = reqparse.RequestParser()
        parser.add_argument('window')

        parser.add_argument('time')

        parser.add_argument('phenomenon')
        parser.add_argument('definition')
        parser.add_argument('center')

        parser.add_argument('version')
        parser.add_argument('begin')
        parser.add_argument('end')

        parser.add_argument('features')
        parser.add_argument('redis_key')
        parser.add_argument('location')
        parser.add_argument('root')
        args = parser.parse_args()

        window = json.loads(args['window'])
        center = json.loads(args['center'])
        feature_sets = {}

        if args['location']:
            current_app.logger.error('Opening location: ' + args['root'])
            features_root = filesystem.open_fs(args['root'])
            settings_file = features_root.open(args['settings_location'])
            settings = settings_file.read()
            features_file = features_root.open(args['features_location'])
            features = features_file.read()
        elif args['redis_key']:
            current_app.logger.error('Opening redis: ' + args['redis_key'])
            key = args['redis_key']
            features = self._redis.get(key).decode('utf-8')
        else:
            current_app.logger.error('Opening features from URL')
            key = None
            features = args['features']

        for feature_set in json.loads(features):
            feature_sets[feature_set['set_id']] = []
            for fs in feature_set['chunk']:
                coordinates = fs['location']['coordinates']
                coordinates.reverse()
                feature_sets[feature_set['set_id']].append(Feature(fs['feature_id'], coordinates))

        if args['version'] != '2':
            times = [int(args['time'])]
        else:
            times = range(0, int(args['end']) - int(args['begin']), self.step)
            #times = [0, 300, 600, 900] #, 900, 1200, 1500, 1800] # RMV

        if args['definition']:
            definition = json.loads(args['definition'])
            risk = phenomena(definition['type'])
            risk_parameters = definition['parameters']
            if not risk_parameters:
                risk_parameters = {}
        else:
            risk = phenomena('surge-elevation')
            risk_parameters = {}

        if risk is None:
            current_app.logger.error('Phenomenon not found')
            return 'Phenomenon not found', 400

        feature_sets, calc_results = self._service.retrieve(times, risk, risk_parameters, feature_sets, center[0], center[1], *window)
        results = []
        for result in calc_results:
            time, points, f, grid = result

            points = {"%d,%d" % k: v for k, v in points.items()}

            if args['begin']:
                time += int(args['begin'])
            results.append({'time': time, 'points': points, 'grid': grid})

        if args['version'] != '2':
            results = results[0]

        feature_arcs = {str(k): [l.healthDict() for l in v] for k, v in feature_sets.items()}

        if key:
            self._redis.delete(key)
            self._redis.set(key + '_result', json.dumps(results).encode('utf-8'))
            results = key + '_result'
            self._redis.expire(results, 600)
            self._redis.set(key + '_feature_arcs', json.dumps(feature_arcs).encode('utf-8'))
            feature_arcs = key + '_feature_arcs'
            self._redis.expire(feature_arcs, 600)

        current_app.logger.error('Finished')
       
        current_app.logger.error({'results': len(results), 'feature_arcs': len(feature_arcs)})
        current_app.logger.error(results)
        return {'results': results, 'feature_arcs': feature_arcs}
