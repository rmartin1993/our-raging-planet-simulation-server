from . import utils

class ConcentricRisk: # limited model showing of distance w concentric damage
    _risk_range = (500, 5000)   # Setting up range of a particular risk
    _time = 10   # Setting up time for a particular risk
    _start_time = None  #  Start time for a particular risk - none in this case

    def __init__(self):
        # gives us a more rectilinear frame of ref
        self._converter = utils.IrishGridConverter()
        self._epicenter = []

    def requires_elevation(self):  # Does this risk require elevation? In this case it doesn't, and returns false.
        return False

    def advance_time(self, time):  # Function for advancing the time
        self._time = time # The time that's passed in through the parameter for this function is assigned to self

    def load(self, logger, center, z, **parameters):  # Load function: loads risk range,logger,epicentre,and parameters
        logger.info("Starting risk range: %f -> %f" % self._risk_range)
        self._logger = logger
        self._epicenter.append((0, self._converter(*center, inverse=False), 0.1))
        self._epicenter.append((18000, (center[0] + 3000, center[1] - 1000), 1.0))
        self._parameters = parameters

    # Are x and y definitely in SI?
    def calculate(self, x, z, i, j): # Calcuating the risk
        x = self._converter(*x, inverse=False) # X is a vector,
        health = 1.0 # Starting value for health

        for time, center, scaling in self._epicenter: # For loop for epicentre
            if time > self._time: # If time is less than the time declared in self do this:
                continue
            distance_squared = (x[0] - center[0]) ** 2 + (x[1] - center[1]) ** 2
            # Do we want quadratic scaling?
            health = min(health, max(0.0, (distance_squared - self._risk_range[0] ** 2) / (self._risk_range[1] ** 2 - self._risk_range[0] ** 2)))

        return 1 - health # Take one away from inital health value

    def finish(self): #  Finish function
        pass
