# http://gis.stackexchange.com/questions/32418/python-script-to-convert-lat-long-to-itm-irish-transverse-mercator

from pyproj import Proj

class IrishGridConverter:  # Class for converting latitude and longitude values into Irish Grid values
    def __init__(self):
        self._proj = Proj(init='epsg:29902')

    def __call__(self, a, b, inverse=True):
        if inverse: # if inverse is set to true
            x, y = a, b
            lo, la = self._proj(x, y, inverse=True)
            c, d = la, lo
        else:
            lo, la = b, a
            x, y = self._proj(lo, la, inverse=False)
            c, d = x, y

        return c, d


def in_box(a, b, x1, y1, x2, y2):
    return (a > x1 and b > y1 and a < x2 and b < y2)
