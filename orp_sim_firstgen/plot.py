import numpy as N
from mpl_toolkits.mplot3d import axes3d
import matplotlib.pyplot as plt
import matplotlib.cm as cm
from scipy import interpolate

#xc, yc, xx, yy, c = N.load('cache/heights.npz', encoding='bytes')

arr = N.loadtxt('app/data/50m.csv', skiprows=1, delimiter=',')
xc, yc, h = zip(*arr)
xi = N.arange(min(xc), max(xc), (max(xc) - min(xc)) / 1000)
yi = N.arange(min(yc), max(yc), (max(yc) - min(yc)) / 1000)
xx, yy = N.meshgrid(xi, yi)

#heights = {(x[0], x[1]): 0 for x in zip(xx.flatten(), yy.flatten())}
# Note these are integer northings/eastings
#for x in arr:
#    c = (x[0], x[1])
#    if c not in heights:
#        app.logger.error(c)
#    heights[c] = x[2] #RMV

zz = interpolate.griddata((xc, yc), N.array(h), (xx, yy), method='nearest')

#@N.vectorize
#def get_height(x, y):
#    return heights[(x, y)]

cached_heights = N.array([xx, yy, zz])
cached_heights.dump('app/cache/heights.npz')
